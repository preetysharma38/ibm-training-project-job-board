package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;

public class TC05_Navigate {

	public static void main(String[] args) {
		// Launching the URL for verifying its Header Image text
		WebDriver driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/jobs");
		
		WebElement Job_Link = driver.findElement(By.linkText("Jobs"));
		//Click the Job link
		Job_Link.click();
		
		//Verify the "Jobs" page is opened
		Assert.assertEquals(driver.getTitle().toString(), "Jobs � Alchemy Jobs");
		driver.close();

	}

}
