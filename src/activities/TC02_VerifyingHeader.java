package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;

public class TC02_VerifyingHeader {

	public static void main(String[] args) {

		// Launching the URL for verifying its Header value
		WebDriver driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/jobs");
		
		WebElement Header = driver.findElement(By.className("entry-title"));
		
		Assert.assertEquals(Header.getText(),"Welcome to Alchemy Jobs");
				
		System.out.println("Header got verified succssfully");
		driver.close();

	}

}
