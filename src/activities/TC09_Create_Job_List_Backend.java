package activities;

import java.util.List;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TC09_Create_Job_List_Backend {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
				
		//Enter Username
		WebElement txt_uname = driver.findElement(By.cssSelector("input#user_login"));
		txt_uname.sendKeys("root");
				
		//Enter Password
		WebElement txt_pass = driver.findElement(By.cssSelector("input#user_pass"));
		txt_pass.sendKeys("pa$$w0rd");
		
		//Click Login
		WebElement btn_login = driver.findElement(By.cssSelector("input#wp-submit"));
		btn_login.click();
		
		//Verify successful login
		Assert.assertEquals("Dashboard � Alchemy Jobs � WordPress", driver.getTitle().toString());
		
		List<WebElement> Job_Listing_Link =  driver.findElements(By.xpath("//div[contains(@class,'wp-menu-name')]"));
		for(WebElement Left_menu : Job_Listing_Link) {
            if(Left_menu.getText().matches("Job Listings"))
            {
            	Left_menu.click();
            	break;
            }            
            }
		
		// Click on Add new option
		WebElement Add_New_Link = driver.findElement(By.cssSelector("a.page-title-action"));
		Add_New_Link.click();
		
        // Enter value in Position text
		WebElement Div_Position = driver.findElement(By.xpath("//div[starts-with(@id,'editor-')]"));
		Div_Position.sendKeys("Testing demo");
		
		//Enter data in Location
		WebElement Loc = driver.findElement(By.xpath("//input[contains(@id,'_job_location')]"));
		Loc.sendKeys("NCR");
		
		//Enter data in Company name
		WebElement Company_Name = driver.findElement(By.xpath("//input[contains(@id,'_company_name')]"));
		Company_Name.sendKeys("IBM");	
	    
		//Clicking Publish button
		WebElement Btn_Publish = driver.findElement(By.xpath("//button[contains(@class,'editor-post-publish-panel__toggle')]"));
		Btn_Publish.click();
	
		//Clicking Publish button second time for publishing
		WebElement Btn_Publish2 = driver.findElement(By.xpath("//button[contains(@class,'editor-post-publish-button')]"));
		Btn_Publish2.click();
		
		//Using wait command for displaying the "Published" word on screen
		WebDriverWait wt = new WebDriverWait(driver, 20);
		wt.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("div.editor-post-publish-panel__header-published"),"Published"));
		
		//Verifying Job Got Published 
		Assert.assertEquals(driver.findElement(By.xpath("//div[contains(@class,'editor-post-publish-panel__header-published')]")).getText(),"Published");
		
	    driver.close();
	}
					
}
