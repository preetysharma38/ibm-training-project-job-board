package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TC06_Apply_Job {

	public static void main(String[] args) throws Exception {
		
				WebDriver driver = new FirefoxDriver();
				driver.get("https://alchemy.hguy.co/jobs");
				
				WebElement Job_Link = driver.findElement(By.linkText("Jobs"));
				//Click the Job link
				Job_Link.click();
				
				WebElement Job_txtbox = driver.findElement(By.xpath("//input[@id='search_keywords']"));
				Job_txtbox.sendKeys("banking financial");
				
				WebElement Search_Btn = driver.findElement(By.xpath("//div[4]/input")); 
				
				Search_Btn.click();
				
				WebDriverWait wait = new WebDriverWait(driver, 30);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("ul.job_listings")));      
							
				WebElement search_link =   driver.findElement(By.xpath("//a[contains(@href,'https://alchemy.hguy.co/jobs/job/banking-financial-analyst/')]")); 
				search_link.click();
				
				WebElement Apply_btn = driver.findElement(By.cssSelector("input.application_button"));				
				Apply_btn.click();
				
				WebElement email_link = driver.findElement(By.cssSelector("a.job_application_email"));
				System.out.println(email_link.getText());
				driver.close();
				
	}

}
