package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TC03_URL_Header_Image {

	public static void main(String[] args) {

		// Launching the URL for verifying its Header Image text
		WebDriver driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/jobs");
		
		WebElement Header_Link = driver.findElement(By.linkText("Alchemy Jobs"));
			// Printing the URL of HEader Image			
		System.out.println(Header_Link.getAttribute("href"));
				driver.close();

	}

}
