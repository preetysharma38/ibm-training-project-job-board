package activities;

import java.util.Random;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class TC07_Post_Job {

	public static void main(String[] args) {
		
		WebDriver driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/jobs");
		
		WebElement Post_Job_Link = driver.findElement(By.linkText("Post a Job"));
		//Click the Job link
		Post_Job_Link.click();
		Random rnd = new Random();
		int rand_int1 = rnd.nextInt(1000);
		
		//Enter Email
		WebElement txt_email = driver.findElement(By.cssSelector("input#create_account_email"));
		txt_email.sendKeys("preetysh"+rand_int1+"@gmail.com");
				
		//Enter Job title
		WebElement txt_job_title = driver.findElement(By.cssSelector("input#job_title"));
		txt_job_title.sendKeys("Senior Tester");
		
		//Select Job Type
		Select Job_type = new Select(driver.findElement(By.id("job_type")));
		Job_type.selectByIndex(1);
		
		//switch to frame and enter description
		driver.switchTo().frame(0);
		WebElement desc = driver.findElement(By.cssSelector("body#tinymce"));
		desc.sendKeys("Testing profile");
		
		driver.switchTo().defaultContent();
		//Enter Application Email
		WebElement application_email = driver.findElement(By.xpath("//input[@id='application']"));
		application_email.sendKeys("jobs@gmail.com");
		
		//Enter Company name
		WebElement company_name = driver.findElement(By.cssSelector("input#company_name"));
		company_name.sendKeys("IBM India");
		
		//Click Preview button
		WebElement btn_Preview = driver.findElement(By.xpath("//input[@class='button' and @value='Preview']"));
		btn_Preview.click();
		
		//Click Preview button
		WebElement btn_Submit_List = driver.findElement(By.xpath("//input[@id='job_preview_submit_button']"));
		btn_Submit_List.click();
					
		//Verify success message
		WebElement success_msg = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div"));
		Assert.assertEquals("Job submitted successfully. Your listing will be visible once approved.", success_msg.getText());
				
		driver.close();
	}

}
