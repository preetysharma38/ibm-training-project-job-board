package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TC011_Search_Job_Xpath {

	public static void main(String[] args) throws InterruptedException {
		
		WebDriver driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/jobs");
		
		//Click the Job link
		WebElement Job_Link = driver.findElement(By.linkText("Jobs"));		
		Job_Link.click();
		
		//Enter the Job search criteria
		WebElement Job_txtbox = driver.findElement(By.xpath("//input[@id='search_keywords']"));
		Job_txtbox.sendKeys("financ");

		//Marking selected Checkbox as unchecked
		WebElement Chkbx_freelance = driver.findElement(By.xpath("//input[@id='job_type_freelance']"));
		if (Chkbx_freelance.isSelected())
		{
		Chkbx_freelance.click();
		}
		
		//Marking selected Checkbox as unchecked
		WebElement Chkbx_Intern = driver.findElement(By.xpath("//input[@id='job_type_internship']"));
		if (Chkbx_Intern.isSelected())
		{
			Chkbx_Intern.click();
		}				
		
		//Marking selected Checkbox as unchecked
		WebElement Chkbx_Part_time = driver.findElement(By.xpath("//input[@id='job_type_part-time']"));
		if (Chkbx_Part_time.isSelected())
		{
			Chkbx_Part_time.click();
		}
		
		//Marking selected Checkbox as unchecked
		WebElement Chkbx_Temp = driver.findElement(By.xpath("//input[@id='job_type_temporary']"));
		if (Chkbx_Temp.isSelected())
		{
			Chkbx_Temp.click();
		}
		//Waiting for filtered result to display
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("li.job-type-full-time")));     
		   
		//Click on job list search
		WebElement job_link =  driver.findElement(By.xpath("//a[@href='https://alchemy.hguy.co/jobs/job/junior-account-manager/']")); 
		job_link.click();

		//For printing Title text in console
		WebElement title_txt =  driver.findElement(By.xpath("//title")); 
		System.out.println("Title :"+title_txt.getAttribute("innerHTML"));
		
		//Click on Apply job
		WebElement btn_apply = driver.findElement(By.xpath("//input[contains(@class,'application_button')]"));
		btn_apply.click();
		
		driver.close();
		
	}

}
