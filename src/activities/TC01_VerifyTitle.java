package activities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;

/**
 * @author PreetySharma
 *
 */
public class TC01_VerifyTitle {

		public static void main(String[] args) {
		// Launching the URL for verifying its Title value
		WebDriver driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/jobs");
		//Matches the Page Title
		Assert.assertEquals(driver.getTitle().toString(), "Alchemy Jobs � Job Board Application");
		
		System.out.println("Title got verified succssfully");
		
		driver.close();		
		
	}

}
