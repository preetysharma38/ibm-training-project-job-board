package activities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TC012_Create_User_CSV_Excel {

	public static void main(String[] args) throws InterruptedException {
		
		WebDriver driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
				
		//Enter Username
		WebElement txt_uname = driver.findElement(By.cssSelector("input#user_login"));
		txt_uname.sendKeys("root");
				
		//Enter Password
		WebElement txt_pass = driver.findElement(By.cssSelector("input#user_pass"));
		txt_pass.sendKeys("pa$$w0rd");
		
		//Click Login
		WebElement btn_login = driver.findElement(By.cssSelector("input#wp-submit"));
		btn_login.click();
		
		//Verify successful login
		Assert.assertEquals("Dashboard � Alchemy Jobs � WordPress", driver.getTitle().toString());
		
		//For finding the User Left menu link
		List<WebElement> Job_Listing_Link =  driver.findElements(By.xpath("//div[contains(@class,'wp-menu-name')]"));
		for(WebElement Left_menu : Job_Listing_Link) {
            if(Left_menu.getText().matches("Users"))
            {
            	Left_menu.click();
            	break;
            }            
            }
		// Click on Add new option
		WebElement Add_New_Link = driver.findElement(By.cssSelector("a.page-title-action"));
		Add_New_Link.click();
		
		//Create webelement for fields Username,Email and password
		WebElement txt_new_uname = driver.findElement(By.cssSelector("input#user_login"));
		WebElement txt_email = driver.findElement(By.cssSelector("input#email"));
			
		//Read Excel File from the location
		try {
            FileInputStream file = new FileInputStream("C:/Selenium/TestAutomation/JOB Board/Test Data.xlsx");

            //Create Workbook 
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            
            //Print the total row count from ExcelSheet
            System.out.println(sheet.getLastRowNum());
            int  rowIndex = sheet.getLastRowNum() ;
            
            //Using for-loop for setting the rowindex value as last row of Excel sheet
            for (rowIndex = sheet.getLastRowNum(); rowIndex >= 0;) {
                final Row row = sheet.getRow(rowIndex);
                if (row != null && row.getCell(0) != null) {
                    rowIndex = rowIndex;
                    break;
                }else {
                	rowIndex = rowIndex-1 ;
                	continue ;
                }
            }
            
            System.out.println("Row index value is :"+rowIndex);
            
            //Enter data into Username and Email text box from excel sheet
            txt_new_uname.sendKeys(sheet.getRow(rowIndex).getCell(0).getStringCellValue().toString());
          	txt_email.sendKeys(sheet.getRow(rowIndex).getCell(1).getStringCellValue().toString());
                                
          //Click Submit password button 
    		WebElement btn_pwd = driver.findElement(By.cssSelector("button.wp-generate-pw"));
    		btn_pwd.click();
              	
    		//Delete the above created record from Excel Sheet in order to avoid duplicate record
          	Row row = workbook.getSheetAt(0).getRow(rowIndex);
            row.removeCell(row.getCell(0));
            row.removeCell(row.getCell(1));
            file.close();
                 
            //Save the modified Excel sheet
         	FileOutputStream New_File = new FileOutputStream("C:/Selenium/TestAutomation/JOB Board/Test Data.xlsx");
          	workbook.write(New_File);
          	New_File.close();
      		
		//Click Add new user button
		WebElement btn_Add = driver.findElement(By.xpath("//input[@id='createusersub']"));
		btn_Add.click();
		
		//Verify success message
		WebElement success_msg = driver.findElement(By.xpath("//div[@id='message']/p"));
		Assert.assertEquals("New user created. Edit user", success_msg.getText());
		
		driver.close();		
   	}
		 catch (Exception e) {
            e.printStackTrace();
        }
	}

}
