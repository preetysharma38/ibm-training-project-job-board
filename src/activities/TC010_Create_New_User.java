package activities;

import java.util.List;
import java.util.Random;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TC010_Create_New_User {

	public static void main(String[] args) {

		WebDriver driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
				
		//Enter Username
		WebElement txt_uname = driver.findElement(By.cssSelector("input#user_login"));
		txt_uname.sendKeys("root");
				
		//Enter Password
		WebElement txt_pass = driver.findElement(By.cssSelector("input#user_pass"));
		txt_pass.sendKeys("pa$$w0rd");
		
		//Click Login
		WebElement btn_login = driver.findElement(By.cssSelector("input#wp-submit"));
		btn_login.click();
		
		//Verify successful login
		Assert.assertEquals("Dashboard � Alchemy Jobs � WordPress", driver.getTitle().toString());
		
		//For finding the User Left menu link
		List<WebElement> Job_Listing_Link =  driver.findElements(By.xpath("//div[contains(@class,'wp-menu-name')]"));
		for(WebElement Left_menu : Job_Listing_Link) {
            if(Left_menu.getText().matches("Users"))
            {
            	Left_menu.click();
            	break;
            }            
            }
		// Click on Add new option
		WebElement Add_New_Link = driver.findElement(By.cssSelector("a.page-title-action"));
		Add_New_Link.click();
		
		Random rnd = new Random();
		int rand_int1 = rnd.nextInt(1000);
		
		//Enter New Username
		WebElement txt_new_uname = driver.findElement(By.cssSelector("input#user_login"));
		txt_new_uname.sendKeys("preety"+rand_int1);
		
		//Enter Email
		WebElement txt_email = driver.findElement(By.cssSelector("input#email"));
		txt_email.sendKeys("preety"+rand_int1+"@gmail.com");
		
		//Click on button password 
		WebElement btn_pwd = driver.findElement(By.cssSelector("button.wp-generate-pw"));
		btn_pwd.click();
				
		//Using wait command for displaying the Hide button on screen
		WebDriverWait wt = new WebDriverWait(driver, 20);
		wt.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("div#pass-strength-result"),"Strong"));
		
		//Enter new user password
		WebElement txt_pwd = driver.findElement(By.cssSelector("input#pass1-text"));
		txt_pwd.sendKeys("preety"+rand_int1);
		
		//Click Add new button
		WebElement btn_Add = driver.findElement(By.xpath("//input[@id='createusersub']"));
		btn_Add.click();
		
		//Verify success message
		WebElement success_msg = driver.findElement(By.xpath("//div[@id='message']/p"));
		Assert.assertEquals("New user created. Edit user", success_msg.getText());

		driver.close();
	}

}
