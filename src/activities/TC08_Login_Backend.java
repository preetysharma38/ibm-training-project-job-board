package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.junit.Assert;

public class TC08_Login_Backend {

	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
				
		//Enter Username
		WebElement txt_uname = driver.findElement(By.cssSelector("input#user_login"));
		txt_uname.sendKeys("root");
				
		//Enter Password
		WebElement txt_pass = driver.findElement(By.cssSelector("input#user_pass"));
		txt_pass.sendKeys("pa$$w0rd");
		
		//Click Login
		WebElement btn_login = driver.findElement(By.cssSelector("input#wp-submit"));
		btn_login.click();
		
		Assert.assertEquals("Dashboard � Alchemy Jobs � WordPress", driver.getTitle().toString());
				
		driver.close();

	}

}
