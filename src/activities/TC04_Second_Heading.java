package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;

public class TC04_Second_Heading {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Launching the URL for verifying its Header Image text
		WebDriver driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/jobs");
		
		WebElement Second_Header = driver.findElement(By.xpath("//article/div/h2"));
		//Matches the Second header exactly
		Assert.assertEquals(Second_Header.getText(), "Quia quis non");
		driver.close();
	}

}
