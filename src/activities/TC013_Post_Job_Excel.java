package activities;
				
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;				
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
				
				public class TC013_Post_Job_Excel {
				
					public static void main(String[] args) {
						// TODO Auto-generated method stub
				WebDriver driver = new FirefoxDriver();
				driver.get("https://alchemy.hguy.co/jobs");
				
				WebElement Post_Job_Link = driver.findElement(By.linkText("Post a Job"));
				//Click the Job link
				Post_Job_Link.click();
						
				//Enter Email
				WebElement txt_email = driver.findElement(By.cssSelector("input#create_account_email"));
				WebElement txt_job_title = driver.findElement(By.cssSelector("input#job_title"));			
				Select Job_type = new Select(driver.findElement(By.id("job_type")));	
				
				WebElement application_email = driver.findElement(By.xpath("//input[@id='application']"));
				WebElement company_name = driver.findElement(By.cssSelector("input#company_name"));
				//Select Job Type
				
				
				//Read Excel File from the location
				try {
				    FileInputStream file = new FileInputStream("C:/Selenium/TestAutomation/JOB Board/Post job Data.xlsx");
				
				//Create Workbook 
				XSSFWorkbook workbook = new XSSFWorkbook(file);
				
				//Get first sheet from the workbook
				XSSFSheet sheet = workbook.getSheetAt(0);
				
				//Print the total row count from ExcelSheet
				System.out.println(sheet.getLastRowNum());
				int  rowIndex = sheet.getLastRowNum() ;
				
				//Using for-loop for setting the rowindex value as last row of Excel sheet
				for (rowIndex = sheet.getLastRowNum(); rowIndex >= 0;) {
				    final Row row = sheet.getRow(rowIndex);
				    if (row != null && row.getCell(0) != null) {
				        rowIndex = rowIndex;
				        break;
				    }else {
				    	rowIndex = rowIndex-1 ;
				    	continue ;
				    }
				}
				
				System.out.println("Row index value is :"+rowIndex);
				
				//Enter data from excel sheet
				txt_email.sendKeys(sheet.getRow(rowIndex).getCell(0).getStringCellValue().toString());
				txt_job_title.sendKeys(sheet.getRow(rowIndex).getCell(1).getStringCellValue().toString());
				Job_type.selectByIndex((int) sheet.getRow(rowIndex).getCell(2).getNumericCellValue());
				  //switch to frame and enter description
				driver.switchTo().frame(0);	
				WebElement desc = driver.findElement(By.cssSelector("body#tinymce"));
				desc.sendKeys(sheet.getRow(rowIndex).getCell(3).getStringCellValue().toString());
				driver.switchTo().defaultContent();
				application_email.sendKeys(sheet.getRow(rowIndex).getCell(4).getStringCellValue().toString());	
				company_name.sendKeys(sheet.getRow(rowIndex).getCell(5).getStringCellValue().toString());
				
				//Store in local variable - Job Title
				String JobTitle = sheet.getRow(rowIndex).getCell(1).getStringCellValue().toString();
				
				//Delete the above created record from Excel Sheet in order to avoid duplicate record
				Row row = workbook.getSheetAt(0).getRow(rowIndex);
				row.removeCell(row.getCell(0));
				row.removeCell(row.getCell(1));
				row.removeCell(row.getCell(2));
				row.removeCell(row.getCell(3));
				row.removeCell(row.getCell(4));
				row.removeCell(row.getCell(5));
				file.close();
				     
				//Save the modified Excel sheet
				FileOutputStream New_File = new FileOutputStream("C:/Selenium/TestAutomation/JOB Board/Post job Data.xlsx");
				workbook.write(New_File);
				New_File.close();			
				
				//Click Preview button
				WebElement btn_Preview = driver.findElement(By.xpath("//input[@class='button' and @value='Preview']"));
				btn_Preview.click();
				
				//Click Preview button
				WebElement btn_Submit_List = driver.findElement(By.xpath("//input[@id='job_preview_submit_button']"));
				btn_Submit_List.click();
							
				//Verify success message
				WebElement success_msg = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div"));
				Assert.assertEquals("Job submitted successfully. Your listing will be visible once approved.", success_msg.getText());
								
				driver.close();
				
				///////////////////////Approve the above Posted Job via Admin Screen///////////////////////////////////////////////////////////////////////
								
				WebDriver driver1 = new FirefoxDriver();
				driver1.get("https://alchemy.hguy.co/jobs/wp-admin");
				
				//Enter Username
				WebElement txt_uname = driver1.findElement(By.cssSelector("input#user_login"));
				txt_uname.sendKeys("root");
						
				//Enter Password
				WebElement txt_pass = driver1.findElement(By.cssSelector("input#user_pass"));
				txt_pass.sendKeys("pa$$w0rd");
				
				//Click Login
				WebElement btn_login = driver1.findElement(By.cssSelector("input#wp-submit"));
				btn_login.click();
				
				Assert.assertEquals("Dashboard � Alchemy Jobs � WordPress", driver1.getTitle().toString());
			
				//For finding the Job Listing Left menu link
				List<WebElement> Job_Listing_Link =  driver1.findElements(By.xpath("//div[contains(@class,'wp-menu-name')]"));
				for(WebElement Left_menu : Job_Listing_Link) {
		            if(Left_menu.getText().matches("Job Listings 1"))
		            {
		            	Left_menu.click();
		            	break;
		            }            
		            } 
								
				WebElement Job_Link = driver1.findElement(By.linkText("Approve"));
				//Click the Job link
				Job_Link.click();
				
				driver1.close();
				
				///////////////////////////////////View Job in List////////////////////////////////////////////////////////
				
				WebDriver driver2 = new FirefoxDriver();
				driver2.get("https://alchemy.hguy.co/jobs");
				
				WebElement Job_Link1 = driver2.findElement(By.linkText("Jobs"));
				//Click the Job link
				Job_Link1.click();
				
				WebElement Job_txtbox = driver2.findElement(By.xpath("//input[@id='search_keywords']"));
				Job_txtbox.sendKeys(JobTitle);
				
				WebElement Search_Btn = driver2.findElement(By.xpath("//div[4]/input")); 
				Search_Btn.click();
				
				WebDriverWait wait = new WebDriverWait(driver2, 30);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("ul.job_listings")));      
				
				System.out.println("Job got listed successfully");
								
				driver2.close();
						}
						 catch (Exception e) {
					            e.printStackTrace();
					        }
					}
				
				}
